package com.yabushan.system.service.impl;

import java.util.List;

import com.yabushan.common.PinYinUtil;
import com.yabushan.common.core.domain.entity.SysUser;
import com.yabushan.common.utils.DateUtils;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.StringUtils;
import com.yabushan.system.domain.UumOrganizationinfo;
import com.yabushan.system.service.ISysUserService;
import com.yabushan.system.service.IUumOrganizationinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.system.mapper.UumUserinfoMapper;
import com.yabushan.system.domain.UumUserinfo;
import com.yabushan.system.service.IUumUserinfoService;

/**
 * 租户用户Service业务层处理
 *
 * @author yabushan
 * @date 2021-04-18
 */
@Service
public class UumUserinfoServiceImpl implements IUumUserinfoService
{
    @Autowired
    private UumUserinfoMapper uumUserinfoMapper;

    @Autowired
    private IUumOrganizationinfoService uumOrganizationinfoService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询租户用户
     *
     * @param employeeId 租户用户ID
     * @return 租户用户
     */
    @Override
    public UumUserinfo selectUumUserinfoById(String employeeId)
    {
        return uumUserinfoMapper.selectUumUserinfoById(employeeId);
    }

    /**
     * 查询租户用户列表
     *
     * @param uumUserinfo 租户用户
     * @return 租户用户
     */
    @Override
    public List<UumUserinfo> selectUumUserinfoList(UumUserinfo uumUserinfo)
    {
        if(StringUtils.isNotEmpty(uumUserinfo.getOrgId())){
            UumOrganizationinfo uumOrganizationinfo1 = uumOrganizationinfoService.selectUumOrganizationinfoById(uumUserinfo.getOrgId());
            uumUserinfo.setWorkOrgId(uumOrganizationinfo1.getOuguid());
            if(uumOrganizationinfo1.getOuLevel()==1){
                //租户
                uumUserinfo.setRegionKey(uumOrganizationinfo1.getRegionKey());
            }else if(uumOrganizationinfo1.getOuLevel()==2){
                //公司
                uumUserinfo.setCompanyId(uumUserinfo.getOrgId());
            }else if(uumOrganizationinfo1.getOuLevel()==3){
                //分公司
                uumUserinfo.setBranchId(uumUserinfo.getOrgId());
            }else if(uumOrganizationinfo1.getOuLevel()==4){
                //部门
                uumUserinfo.setDepartmentId(uumUserinfo.getOrgId());
            }else if(uumOrganizationinfo1.getOuLevel()==5){
                //科室
                uumUserinfo.setOfficeId(uumUserinfo.getOrgId());
            }else if(uumOrganizationinfo1.getOuLevel()==6){
                //团队
                uumUserinfo.setGroupId(uumUserinfo.getOrgId());
            }else if(uumOrganizationinfo1.getOuLevel()==7){
                //小组
                uumUserinfo.setTeamId(uumUserinfo.getOrgId());
            }
        }
        return uumUserinfoMapper.selectUumUserinfoList(uumUserinfo);
    }

    /**
     * 新增租户用户
     *
     * @param uumUserinfo 租户用户
     * @return 结果
     */
    @Override
    public int insertUumUserinfo(UumUserinfo uumUserinfo)
    {
        //传入的COMPANYID 为用户的归属组织，需要根据归属组织填充该用户的组织信息
        //getFullSpell
        UumOrganizationinfo orgfo = uumOrganizationinfoService.selectUumOrganizationinfoById(uumUserinfo.getOrgId());
        UumOrganizationinfo workOrg=uumOrganizationinfoService.selectUumOrganizationinfoById(uumUserinfo.getWorkOrgId());
        uumUserinfo.setEmployeeId(orgfo.getRegionKey()+ StringUtils.getUUID());
        //生成员工账号，员工姓名拼音+数字，跳过4，14，24，34，44，54，64，74，84，94
        String loginId = PinYinUtil.getFullSpell(uumUserinfo.getFullName());
        Integer num=1;
        String loginIdNum=loginId;
        while(getLoginId(loginIdNum)){
            num++;
            loginIdNum=loginId+num.toString();
        }
        uumUserinfo.setLoginId(loginIdNum);
        //设置其他信息
        uumUserinfo.setRegionKey(orgfo.getRegionKey());
        uumUserinfo.setRegionName(orgfo.getRegionName());
        uumUserinfo.setCompanyId(orgfo.getCompanyId());
        uumUserinfo.setCompanyName(orgfo.getCompanyName());
        uumUserinfo.setBranchId(orgfo.getBranchId());
        uumUserinfo.setBranchName(orgfo.getBranchName());
        uumUserinfo.setDepartmentId(orgfo.getDepartmentId());
        uumUserinfo.setDepartmentName(orgfo.getDepartmentName());
        uumUserinfo.setOfficeId(orgfo.getOfficeId());
        uumUserinfo.setOfficeName(orgfo.getOfficeName());
        uumUserinfo.setTeamId(orgfo.getTeamId());
        uumUserinfo.setTeamName(orgfo.getTeamName());
        uumUserinfo.setGroupId(orgfo.getGroupId());
        uumUserinfo.setGroupName(orgfo.getGroupName());
        uumUserinfo.setWorkOrgId(workOrg.getOuguid());
        uumUserinfo.setWorkOrgName(workOrg.getOuName());
        uumUserinfo.setOrgIdName(orgfo.getOuName());

        //加入时间日期信息
        uumUserinfo.setCreateTime(DateUtils.getNowDate());
        uumUserinfo.setUpdateTime(DateUtils.getNowDate());
        uumUserinfo.setCreateBy(SecurityUtils.getUsername());
        uumUserinfo.setUpdateBy(SecurityUtils.getUsername());


        //插入一条数据到系统 用户表
        SysUser sysUser = new SysUser();
        sysUser.setDeptId(110L);
        sysUser.setUserName(uumUserinfo.getLoginId());
        sysUser.setNickName(uumUserinfo.getFullName());
        sysUser.setEmail(uumUserinfo.getWorkEmail());
        sysUser.setPhonenumber(uumUserinfo.getTelPhone());
        sysUser.setRemark(uumUserinfo.getRegionKey());
        userService.insertUser(sysUser);

        return uumUserinfoMapper.insertUumUserinfo(uumUserinfo);
    }

    private Boolean getLoginId(String loginId){
        UumUserinfo uumUserinfo = uumUserinfoMapper.selectUumUserinfoByLoginId(loginId);
        if(uumUserinfo!=null){
            return true;
        } else{
            return false;
        }

    }



    /**
     * 修改租户用户
     *
     * @param uumUserinfo 租户用户
     * @return 结果
     */
    @Override
    public int updateUumUserinfo(UumUserinfo uumUserinfo)
    {
        UumOrganizationinfo orgfo = uumOrganizationinfoService.selectUumOrganizationinfoById(uumUserinfo.getOrgId());
        UumOrganizationinfo workOrg=uumOrganizationinfoService.selectUumOrganizationinfoById(uumUserinfo.getWorkOrgId());
        //设置其他信息
        uumUserinfo.setRegionKey(orgfo.getRegionKey());
        uumUserinfo.setRegionName(orgfo.getRegionName());
        uumUserinfo.setCompanyId(orgfo.getCompanyId());
        uumUserinfo.setCompanyName(orgfo.getCompanyName());
        uumUserinfo.setBranchId(orgfo.getBranchId());
        uumUserinfo.setBranchName(orgfo.getBranchName());
        uumUserinfo.setDepartmentId(orgfo.getDepartmentId());
        uumUserinfo.setDepartmentName(orgfo.getDepartmentName());
        uumUserinfo.setOfficeId(orgfo.getOfficeId());
        uumUserinfo.setOfficeName(orgfo.getOfficeName());
        uumUserinfo.setTeamId(orgfo.getTeamId());
        uumUserinfo.setTeamName(orgfo.getTeamName());
        uumUserinfo.setGroupId(orgfo.getGroupId());
        uumUserinfo.setGroupName(orgfo.getGroupName());
        uumUserinfo.setWorkOrgId(workOrg.getOuguid());
        uumUserinfo.setWorkOrgName(workOrg.getOuName());
        uumUserinfo.setOrgIdName(orgfo.getOuName());
        //加入时间日期信息
        uumUserinfo.setUpdateTime(DateUtils.getNowDate());
        uumUserinfo.setUpdateBy(SecurityUtils.getUsername());
        return uumUserinfoMapper.updateUumUserinfo(uumUserinfo);
    }

    /**
     * 批量删除租户用户
     *
     * @param employeeIds 需要删除的租户用户ID
     * @return 结果
     */
    @Override
    public int deleteUumUserinfoByIds(String[] employeeIds)
    {
        return uumUserinfoMapper.deleteUumUserinfoByIds(employeeIds);
    }

    /**
     * 删除租户用户信息
     *
     * @param employeeId 租户用户ID
     * @return 结果
     */
    @Override
    public int deleteUumUserinfoById(String employeeId)
    {
        return uumUserinfoMapper.deleteUumUserinfoById(employeeId);
    }
}
