package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.NgsAttendance;
import com.yabushan.system.service.INgsAttendanceService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 考勤记录Controller
 *
 * @author yabushan
 * @date 2021-06-06
 */
@RestController
@RequestMapping("/system/attendance")
public class NgsAttendanceController extends BaseController
{
    @Autowired
    private INgsAttendanceService ngsAttendanceService;

    /**
     * 查询考勤记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:list')")
    @GetMapping("/list")
    public TableDataInfo list(NgsAttendance ngsAttendance)
    {
        startPage();
        List<NgsAttendance> list = ngsAttendanceService.selectNgsAttendanceList(ngsAttendance);
        return getDataTable(list);
    }

    /**
     * 导出考勤记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:export')")
    @Log(title = "考勤记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(NgsAttendance ngsAttendance)
    {
        List<NgsAttendance> list = ngsAttendanceService.selectNgsAttendanceList(ngsAttendance);
        ExcelUtil<NgsAttendance> util = new ExcelUtil<NgsAttendance>(NgsAttendance.class);
        return util.exportExcel(list, "attendance");
    }

    /**
     * 获取考勤记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:query')")
    @GetMapping(value = "/{atdId}")
    public AjaxResult getInfo(@PathVariable("atdId") String atdId)
    {
        return AjaxResult.success(ngsAttendanceService.selectNgsAttendanceById(atdId));
    }

    /**
     * 新增考勤记录
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:add')")
    @Log(title = "考勤记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NgsAttendance ngsAttendance)
    {
        return toAjax(ngsAttendanceService.insertNgsAttendance(ngsAttendance));
    }

    /**
     * 修改考勤记录
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:edit')")
    @Log(title = "考勤记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NgsAttendance ngsAttendance)
    {
        return toAjax(ngsAttendanceService.updateNgsAttendance(ngsAttendance));
    }

    /**
     * 删除考勤记录
     */
    @PreAuthorize("@ss.hasPermi('system:attendance:remove')")
    @Log(title = "考勤记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{atdIds}")
    public AjaxResult remove(@PathVariable String[] atdIds)
    {
        return toAjax(ngsAttendanceService.deleteNgsAttendanceByIds(atdIds));
    }
}
