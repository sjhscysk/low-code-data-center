package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.LpkCustomer;
import com.yabushan.system.service.ILpkCustomerService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 外卖Controller
 *
 * @author yabushan
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/system/customer")
public class LpkCustomerController extends BaseController
{
    @Autowired
    private ILpkCustomerService lpkCustomerService;

    /**
     * 查询外卖列表
     */
    @PreAuthorize("@ss.hasPermi('system:customer:list')")
    @GetMapping("/list")
    public TableDataInfo list(LpkCustomer lpkCustomer)
    {
        startPage();
        List<LpkCustomer> list = lpkCustomerService.selectLpkCustomerList(lpkCustomer);
        return getDataTable(list);
    }

    /**
     * 导出外卖列表
     */
    @PreAuthorize("@ss.hasPermi('system:customer:export')")
    @Log(title = "外卖", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(LpkCustomer lpkCustomer)
    {
        List<LpkCustomer> list = lpkCustomerService.selectLpkCustomerList(lpkCustomer);
        ExcelUtil<LpkCustomer> util = new ExcelUtil<LpkCustomer>(LpkCustomer.class);
        return util.exportExcel(list, "customer");
    }

    /**
     * 获取外卖详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:customer:query')")
    @GetMapping(value = "/{keyId}")
    public AjaxResult getInfo(@PathVariable("keyId") Long keyId)
    {
        return AjaxResult.success(lpkCustomerService.selectLpkCustomerById(keyId));
    }

    /**
     * 新增外卖
     */
   // @PreAuthorize("@ss.hasPermi('system:customer:add')")
    //@Log(title = "外卖", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LpkCustomer lpkCustomer)
    {
        return toAjax(lpkCustomerService.insertLpkCustomer(lpkCustomer));
    }

    /**
     * 修改外卖
     */
    @PreAuthorize("@ss.hasPermi('system:customer:edit')")
    @Log(title = "外卖", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LpkCustomer lpkCustomer)
    {
        return toAjax(lpkCustomerService.updateLpkCustomer(lpkCustomer));
    }

    /**
     * 删除外卖
     */
    @PreAuthorize("@ss.hasPermi('system:customer:remove')")
    @Log(title = "外卖", businessType = BusinessType.DELETE)
	@DeleteMapping("/{keyIds}")
    public AjaxResult remove(@PathVariable Long[] keyIds)
    {
        return toAjax(lpkCustomerService.deleteLpkCustomerByIds(keyIds));
    }
}
