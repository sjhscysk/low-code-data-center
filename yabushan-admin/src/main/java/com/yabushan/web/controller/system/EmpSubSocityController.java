package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubSocity;
import com.yabushan.system.service.IEmpSubSocityService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工家庭关系子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/socity")
public class EmpSubSocityController extends BaseController
{
    @Autowired
    private IEmpSubSocityService empSubSocityService;

    /**
     * 查询员工家庭关系子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:socity:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubSocity empSubSocity)
    {
        startPage();
        List<EmpSubSocity> list = empSubSocityService.selectEmpSubSocityList(empSubSocity);
        return getDataTable(list);
    }

    /**
     * 导出员工家庭关系子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:socity:export')")
    @Log(title = "员工家庭关系子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubSocity empSubSocity)
    {
        List<EmpSubSocity> list = empSubSocityService.selectEmpSubSocityList(empSubSocity);
        ExcelUtil<EmpSubSocity> util = new ExcelUtil<EmpSubSocity>(EmpSubSocity.class);
        return util.exportExcel(list, "socity");
    }

    /**
     * 获取员工家庭关系子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:socity:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubSocityService.selectEmpSubSocityById(recId));
    }

    /**
     * 新增员工家庭关系子集
     */
    @PreAuthorize("@ss.hasPermi('system:socity:add')")
    @Log(title = "员工家庭关系子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubSocity empSubSocity)
    {
        return toAjax(empSubSocityService.insertEmpSubSocity(empSubSocity));
    }

    /**
     * 修改员工家庭关系子集
     */
    @PreAuthorize("@ss.hasPermi('system:socity:edit')")
    @Log(title = "员工家庭关系子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubSocity empSubSocity)
    {
        return toAjax(empSubSocityService.updateEmpSubSocity(empSubSocity));
    }

    /**
     * 删除员工家庭关系子集
     */
    @PreAuthorize("@ss.hasPermi('system:socity:remove')")
    @Log(title = "员工家庭关系子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubSocityService.deleteEmpSubSocityByIds(recIds));
    }
}
