package com.yabushan.web.controller;

import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.core.domain.entity.SysUser;
import com.yabushan.common.core.domain.model.LoginUser;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.common.utils.ServletUtils;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.web.model.common.ResponseBean;
import com.yabushan.web.model.entity.YmxCustomerInformationInfo;
import com.yabushan.web.model.entity.YmxGiftInfo;
import com.yabushan.web.model.entity.YmxOrderInfo;
import com.yabushan.web.model.vo.*;
import com.yabushan.web.service.LandingPageApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description LandingPageApiController
 * @Author huihui
 * @Date 2021-03-29 11:33:09
 * @Version 1.0
 * ${yabushan}/
 */
@RequestMapping("ymx-api/landing_page")
@RestController
@Api(tags = "着陆页模块")
public class LandingPageApiController extends BaseController {

    @Resource
    private LandingPageApiService landingPageApiService;

    @ApiOperation(value = "订单号匹配", position = 10)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderInfoVO", value = "订单信息", required = false, dataType = "YmxOrderInfoVO")
    })
    //@PreAuthorize("@ss.hasPermi('ymx:landing:match')")
    @Log(title = "订单号匹配", businessType = BusinessType.OTHER)
    @PostMapping(value = "selectOrderMatching")
    @ResponseBody
    public ResponseBean<List<YmxOrderInfo>> selectOrderMatching(@RequestBody YmxOrderInfoVO orderInfoVO) {
        List<YmxOrderInfo> list = landingPageApiService.selectOrderMatching(orderInfoVO);
        return ResponseBean.success("订单匹配成功", list);
    }

    @ApiOperation(value = "录入客户个人信息", position = 20)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "infoVo", value = "客户信息", required = false, dataType = "YmxCustomerInformationInfoVo")
    })
    //@PreAuthorize("@ss.hasPermi('ymx:landing:insertCustomer')")
    @Log(title = "录入客户个人信息", businessType = BusinessType.INSERT)
    @PostMapping(value = "insertCustomerInformationInfo")
    @ResponseBody
    public ResponseBean insertCustomerInformationInfo(@RequestBody YmxCustomerInformationInfoVo infoVo) {
        int result = landingPageApiService.insertCustomerInformationInfo(infoVo);
        if (result == 1) {
            return ResponseBean.success("录入客户个人信息成功", result);
        }
        return ResponseBean.fail("录入客户个人信息失败");
    }

    @ApiOperation(value = "礼物查询列表", position = 30)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "giftInfoVO", value = "礼物查询列表信息", required = false, dataType = "YmxOrderGiftInfoVO")
    })
    //@PreAuthorize("@ss.hasPermi('ymx:landing:selectGift')")
    @Log(title = "礼物查询列表", businessType = BusinessType.OTHER)
    @PostMapping(value = "selectGiftInfo")
    @ResponseBody
    public ResponseBean<List<YmxGiftInfo>> selectGiftInfo(@RequestBody YmxOrderGiftInfoVO giftInfoVO) {
        List<YmxGiftInfo> list = landingPageApiService.selectGiftInfo(giftInfoVO);
        return ResponseBean.success("礼物查询列表成功", list);
    }

    @ApiOperation(value = "记录客户所选礼物", position = 40)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "giftInfoVO", value = "客户信息", required = false, dataType = "YmxCustomerGiftInfoVO")
    })
    //@PreAuthorize("@ss.hasPermi('ymx:landing:recordGifts')")
    @Log(title = "记录客户所选礼物", businessType = BusinessType.INSERT)
    @PostMapping(value = "recordCustomerGifts")
    @ResponseBody
    public ResponseBean recordCustomerGifts(@RequestBody YmxCustomerGiftInfoVO giftInfoVO) {
        int result = landingPageApiService.recordCustomerGifts(giftInfoVO);
        if (result == 1) {
            return ResponseBean.success("记录客户所选礼物成功", result);
        }
        return ResponseBean.fail("记录客户所选礼物失败");
    }

    @ApiOperation(value = "记录客户所选星级或评论", position = 50)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentInfoVO", value = "客户信息", required = false, dataType = "YmxCommentInfoVO")
    })
    //@PreAuthorize("@ss.hasPermi('ymx:landing:recordStar')")
    @Log(title = "记录客户所选星级或评论", businessType = BusinessType.INSERT)
    @PostMapping(value = "recordCustomerStarRating")
    @ResponseBody
    public ResponseBean recordCustomerStarRating(@RequestBody YmxCommentInfoVO commentInfoVO) {
        int result = landingPageApiService.recordCustomerStarRating(commentInfoVO);
        if (result == 1) {
            return ResponseBean.success("记录客户所选星级或评论成功", result);
        }
        return ResponseBean.fail("记录客户所选星级或评论失败");
    }

    @Log(title = "批量导入客户信息", businessType = BusinessType.IMPORT)
    //@PreAuthorize("@ss.hasPermi('system:user:import')")
    @PostMapping("/importData")
    @ApiOperation(value = "批量导入客户信息")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<YmxCustomerInformationInfo> util = new ExcelUtil<YmxCustomerInformationInfo>(YmxCustomerInformationInfo.class);
        List<YmxCustomerInformationInfo> userList = util.importExcel(file.getInputStream());
        System.out.println(userList);
       // LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        //String operName = loginUser.getUsername();
        //String message = userService.importUser(userList, updateSupport, operName);
        return AjaxResult.success("批量导入客户信息");
    }
}
