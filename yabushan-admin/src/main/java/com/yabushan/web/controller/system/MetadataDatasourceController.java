package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.MetadataDatasource;
import com.yabushan.system.service.IMetadataDatasourceService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 数据源Controller
 *
 * @author yabushan
 * @date 2021-01-16
 */
@RestController
@RequestMapping("/system/datasource")
public class MetadataDatasourceController extends BaseController
{
    @Autowired
    private IMetadataDatasourceService metadataDatasourceService;

    /**
     * 查询数据源列表
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:list')")
    @GetMapping("/list")
    public TableDataInfo list(MetadataDatasource metadataDatasource)
    {
        startPage();
        List<MetadataDatasource> list = metadataDatasourceService.selectMetadataDatasourceList(metadataDatasource);
        return getDataTable(list);
    }

    /**
     * 导出数据源列表
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:export')")
    @Log(title = "数据源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(MetadataDatasource metadataDatasource)
    {
        List<MetadataDatasource> list = metadataDatasourceService.selectMetadataDatasourceList(metadataDatasource);
        ExcelUtil<MetadataDatasource> util = new ExcelUtil<MetadataDatasource>(MetadataDatasource.class);
        return util.exportExcel(list, "datasource");
    }

    /**
     * 获取数据源详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:query')")
    @GetMapping(value = "/{datasourceId}")
    public AjaxResult getInfo(@PathVariable("datasourceId") String datasourceId)
    {
        return AjaxResult.success(metadataDatasourceService.selectMetadataDatasourceById(datasourceId));
    }

    /**
     * 新增数据源
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:add')")
    @Log(title = "数据源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add()
    {
        return toAjax(metadataDatasourceService.insertMetadataDatasource());
    }

    /**
     * 修改数据源
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:edit')")
    @Log(title = "数据源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MetadataDatasource metadataDatasource)
    {
        return toAjax(metadataDatasourceService.updateMetadataDatasource(metadataDatasource));
    }

    /**
     * 删除数据源
     */
    @PreAuthorize("@ss.hasPermi('system:datasource:remove')")
    @Log(title = "数据源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{datasourceIds}")
    public AjaxResult remove(@PathVariable String[] datasourceIds)
    {
        return toAjax(metadataDatasourceService.deleteMetadataDatasourceByIds(datasourceIds));
    }
}
