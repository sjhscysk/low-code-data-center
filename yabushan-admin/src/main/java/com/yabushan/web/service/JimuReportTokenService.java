/*
package com.yabushan.web.service;

*/
/**
 * @Author yabushan
 * @Date 2021/7/1 14:33
 * @Version 1.0
 *//*

import com.yabushan.common.constant.Constants;
import com.yabushan.common.core.domain.model.LoginUser;
import com.yabushan.common.utils.AesEncryptUtil;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.sign.Base64;
import com.yabushan.form.domain.JimuReport;
import com.yabushan.form.service.IJimuReportService;
import com.yabushan.framework.web.service.TokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.SneakyThrows;
import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class JimuReportTokenService implements JmReportTokenServiceI {



    @Autowired
    private IJimuReportService service;

    @SneakyThrows
    @Override
    public String getToken(HttpServletRequest request) {
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String userName =split[1]+"";
        userName=AesEncryptUtil.desEncrypt(userName)+"";
        String type = split[0]+"";
        if("addExcelReport".equals(type.trim())){
            return userName.trim();
        }
        return token;
    }

    @Override
    public String getUsername(String token) {
      //  String usernameFromToken = tokenService.getUsernameFromToken(token);
        return token;
    }

    @SneakyThrows
    @Override
    public Boolean verifyToken(String token) {
        if(token.contains("addExcelReport")){
            return true;
        }
       // String usernameFromToken = tokenService.getUsernameFromToken(token);
        String[] split = token.split("-");

        if(split.length==1){return true;}
        String id = null;
        id = split[0];
        String userName=AesEncryptUtil.desEncrypt(split[1])+"";
        if("admin".equals(userName.trim())){
            return true;
        }else if("addExcelReport".equals(id.trim())){
            //进入报表设计器
            return true;
        }else {
            JimuReport report = new JimuReport();
            report.setCode(id.replace("custom_","")+"");
            report.setId(id+"");
            List<JimuReport> jimuReports = service.selectJimuReportList(report);
            if(userName.equals(jimuReports.get(0).getCreateBy())){
                return  true;
            }
        }
        return false;
       // return TokenUtils.verifyToken(token, sysBaseAPI, redisUtil);
    }

    @Override
    public Map<String, Object> getUserInfo(String token) {
        String username = SecurityUtils.getUsername();

        Map<String, Object> map = new HashMap<String, Object>();
        //设置账号名
        map.put("sysUserCode",  SecurityUtils.getUsername());
        //设置部门编码
        map.put("sysOrgCode",  SecurityUtils.getLoginUser().getUser().getDeptId());
        // 将所有信息存放至map 解析sql会根据map的键值解析,可自定义其他值
        return map;
    }


}
*/
