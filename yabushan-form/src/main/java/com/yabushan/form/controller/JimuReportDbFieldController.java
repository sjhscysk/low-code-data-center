package com.yabushan.form.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.form.domain.JimuReportDbField;
import com.yabushan.form.service.IJimuReportDbFieldService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 报表Controller
 *
 * @author yabushan
 * @date 2021-07-03
 */
@RestController
@RequestMapping("/form/reportfield")
public class JimuReportDbFieldController extends BaseController
{
    @Autowired
    private IJimuReportDbFieldService jimuReportDbFieldService;

    /**
     * 查询报表列表
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:list')")
    @GetMapping("/list")
    public TableDataInfo list(JimuReportDbField jimuReportDbField)
    {
        startPage();
        List<JimuReportDbField> list = jimuReportDbFieldService.selectJimuReportDbFieldList(jimuReportDbField);
        return getDataTable(list);
    }

    /**
     * 导出报表列表
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:export')")
    @Log(title = "报表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(JimuReportDbField jimuReportDbField)
    {
        List<JimuReportDbField> list = jimuReportDbFieldService.selectJimuReportDbFieldList(jimuReportDbField);
        ExcelUtil<JimuReportDbField> util = new ExcelUtil<JimuReportDbField>(JimuReportDbField.class);
        return util.exportExcel(list, "reportfield");
    }

    /**
     * 获取报表详细信息
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(jimuReportDbFieldService.selectJimuReportDbFieldById(id));
    }

    /**
     * 新增报表
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:add')")
    @Log(title = "报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody JimuReportDbField jimuReportDbField)
    {
        return toAjax(jimuReportDbFieldService.insertJimuReportDbField(jimuReportDbField));
    }

    /**
     * 修改报表
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:edit')")
    @Log(title = "报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody JimuReportDbField jimuReportDbField)
    {
        return toAjax(jimuReportDbFieldService.updateJimuReportDbField(jimuReportDbField));
    }

    /**
     * 删除报表
     */
    @PreAuthorize("@ss.hasPermi('form:reportfield:remove')")
    @Log(title = "报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(jimuReportDbFieldService.deleteJimuReportDbFieldByIds(ids));
    }
}
