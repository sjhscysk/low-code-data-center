package com.yabushan.form.mapper;

import java.util.List;
import com.yabushan.form.domain.AutoUserTables;

/**
 * 用户建等级Mapper接口
 * 
 * @author yabushan
 * @date 2021-08-06
 */
public interface AutoUserTablesMapper 
{
    /**
     * 查询用户建等级
     * 
     * @param tId 用户建等级ID
     * @return 用户建等级
     */
    public AutoUserTables selectAutoUserTablesById(Long tId);

    /**
     * 查询用户建等级列表
     * 
     * @param autoUserTables 用户建等级
     * @return 用户建等级集合
     */
    public List<AutoUserTables> selectAutoUserTablesList(AutoUserTables autoUserTables);

    /**
     * 新增用户建等级
     * 
     * @param autoUserTables 用户建等级
     * @return 结果
     */
    public int insertAutoUserTables(AutoUserTables autoUserTables);

    /**
     * 修改用户建等级
     * 
     * @param autoUserTables 用户建等级
     * @return 结果
     */
    public int updateAutoUserTables(AutoUserTables autoUserTables);

    /**
     * 删除用户建等级
     * 
     * @param tId 用户建等级ID
     * @return 结果
     */
    public int deleteAutoUserTablesById(Long tId);

    /**
     * 批量删除用户建等级
     * 
     * @param tIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteAutoUserTablesByIds(Long[] tIds);
}
