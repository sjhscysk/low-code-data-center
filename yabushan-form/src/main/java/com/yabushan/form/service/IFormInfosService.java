package com.yabushan.form.service;

import java.util.List;
import java.util.Map;

import com.yabushan.form.domain.FormInfos;
import com.yabushan.form.domain.JimuReport;
import com.yabushan.form.domain.JimuReportDb;
import com.yabushan.form.domain.JimuReportDbField;

/**
 * 自定义表单Service接口
 *
 * @author yabushan
 * @date 2021-06-26
 */
public interface IFormInfosService
{
    /**
     * 查询自定义表单
     *
     * @param id 自定义表单ID
     * @return 自定义表单
     */
    public FormInfos selectFormInfosById(Long id);

    /**
     * 查询自定义表单列表
     *
     * @param formInfos 自定义表单
     * @return 自定义表单集合
     */
    public List<FormInfos> selectFormInfosList(FormInfos formInfos);

    /**
     * 新增自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    public int insertFormInfos(FormInfos formInfos);

    /**
     * 修改自定义表单
     *
     * @param formInfos 自定义表单
     * @return 结果
     */
    public int updateFormInfos(FormInfos formInfos);

    /**
     * 批量删除自定义表单
     *
     * @param ids 需要删除的自定义表单ID
     * @return 结果
     */
    public int deleteFormInfosByIds(Long[] ids);

    /**
     * 删除自定义表单信息
     *
     * @param id 自定义表单ID
     * @return 结果
     */
    public int deleteFormInfosById(Long id);



    //生成数据库表
    public void  createTable(String sql, FormInfos formInfos, JimuReport report, JimuReportDb reportDb, List<JimuReportDbField> fields);

    public void dynamicsInsert(String sql);

    public void dynamicsUpdate(String sql);

    public List<Map> dynamicsSelect(String sql);

    public void dynamicsDelete(String sql);
}
